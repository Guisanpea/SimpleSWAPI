package es.uma.rysd.entities;

public class Person {
	public String name;
	public String birth_year;
	public String eye_color;
	public String gender;
	public String hair_color;
	public String height;
	public String mass;
	public String skin_color;
	public String homeworld;
	public String[] films;
	
	public Film[] movies;
	public Planet homeplanet;
	
	public String toString(){
		String text = name + " ("+ gender +") nacio en el a�o " + birth_year+" en "+homeplanet+"\n";
		text += "Pesa: " + mass + " Kg y mide: " + height + " cm\n";
		text += "Aparece en:\n";
		for(Film f: movies){
			text += "- "+f+"\n";
		}
		return text;		
	}
}
