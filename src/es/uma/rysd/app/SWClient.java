package es.uma.rysd.app;

import com.google.gson.Gson;
import es.uma.rysd.entities.Film;
import es.uma.rysd.entities.Person;
import es.uma.rysd.entities.Planet;
import es.uma.rysd.entities.SearchResponse;
import org.apache.commons.io.IOUtils;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Arrays;

import static java.nio.charset.StandardCharsets.UTF_8;

public class SWClient {
    public Person search(String name) throws IOException {
        Person person = null;
        HttpsURLConnection connection = getApiConnection(name);

        int responseCode = connection.getResponseCode();
        boolean correct = responseCode >= 200 && responseCode < 300;

        if (correct) {
            SearchResponse lp = parseConnectionJson(connection, SearchResponse.class);
            connection.disconnect();
            person = lp.results[0];
            person.movies = getPersonFilms(person);
            person.homeplanet = getPersonPlanet(person);
        }
        return person;
    }

    private Planet getPersonPlanet(Person person) throws IOException {
        return parseConnectionJson(
              createConnection(new URL(person.homeworld)),
              Planet.class
        );
    }

    private Film[] getPersonFilms(Person person) {
        return Arrays.stream(person.films)
              .map(filmUrl -> createConnection(safeUrl(filmUrl)))
              .map(connection -> parseConnectionJson(connection, Film.class))
              .toArray(Film[]::new);
    }

    private URL safeUrl(String filmUrl) {
        URL returned = null;

        try {
            returned = new URL(filmUrl);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return returned;
    }

    private <T> T parseConnectionJson(HttpsURLConnection connection, Class<T> responseClass) {
        Gson parser = new Gson();
        InputStream in = null;
        try {
            in = connection.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // In case you want to check the output
        // printResultToFile(in);
        T result = parser.fromJson(new InputStreamReader(in), responseClass);
        return result;
    }

    private void printResultToFile(InputStream in) {
        try {
            FileOutputStream out = new FileOutputStream("result.txt");
            IOUtils.copy(in, out);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private HttpsURLConnection getApiConnection(String name) throws IOException {
        URL getPeople = getUrl(name);
        return createConnection(getPeople);
    }

    private URL getUrl(String name) throws MalformedURLException, UnsupportedEncodingException {
        String requestUrl = "https://swapi.co/api/people/" +
              "?search=" +
              URLEncoder.encode(name, UTF_8.toString());
        return new URL(requestUrl);
    }

    private HttpsURLConnection createConnection(URL getPeople) {
        HttpsURLConnection connection = null;
        try {
            connection = ((HttpsURLConnection)
                  getPeople.openConnection());
            connection.setRequestProperty("Accept", "application/json");
            String app_name = "RESTWars";
            connection.setRequestProperty("User-Agent", app_name);

            connection.setRequestMethod("GET");
        } catch (IOException e) {
            e.printStackTrace();
        }

        return connection;
    }
}
            
          