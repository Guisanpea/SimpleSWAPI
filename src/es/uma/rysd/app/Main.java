package es.uma.rysd.app;

import java.io.IOException;
import java.util.Scanner;

import es.uma.rysd.entities.Person;

public class Main {

    public static void main(String[] args) {
        SWClient sw = new SWClient();
        Scanner sc = new Scanner(System.in);
        String answer;

        try {
            do {
                System.out.println("Personaje a buscar: ");
                String name = sc.nextLine();

                Person p = sw.search(name);
                System.out.println(p);

                System.out.println("\nDesea buscar otro personaje (s/n)?");
                answer = sc.nextLine();
            } while (answer.equals("s"));
        } catch (IOException e) {
            System.err.println("There has been an error processing person");
        }

        System.out.println("Finishing...");
        sc.close();
    }
}
